<?php

    require_once('bdd_connexion.php');
    
    $id = $_SESSION['id'];
    if(isset($id)){

    } else {
        var_dump($id);
        // header('Location: ../index.php');
    }

    function allFiles($db_connect){
        #code
        try {
            $query = $db_connect->prepare('SELECT * FROM files'); 
            if(!$query->execute()){
                throw new Exception("<br>PDO::errorInfo():<br> Erreur durant la récupération des fichiers<br>");
            }
            return $query->fetchAll(PDO::FETCH_ASSOC);
        } catch  (Exception $e){
            echo $e->getMessage();
            var_dump($db_connect->errorInfo());
            die($e->getMessage());
        }
    }

    function oneFile($db_connect, $id){
        #code
        try {
            $query = $db_connect->prepare('SELECT * FROM files WHERE _id = :ID'); 
            $query->execute(array("ID" => $id));
            $result = $query->fetch(PDO::FETCH_ASSOC);
            if(!$result){
                throw new Exception("<br>PDO::errorInfo():<br> Erreur durant la récupération du fichier<br>");
            }
            return $result;
        } catch  (Exception $e){
            echo $e->getMessage();
            var_dump($db_connect->errorInfo());
            die($e->getMessage());
        }
    }

    function creationFile($db_connect,$tabAgs){
        try{
            #1st block : Créer un id pour le nouvel utilisateur
            $query = $db_connect->prepare('SELECT count(*) as filesNumber FROM files');
            $query->execute();
            $searchnbuser = $query->fetch(PDO::FETCH_ASSOC);
            $id = $searchnbuser? $searchnbuser['filesNumber']+1: 1;
            #2nd block: Création du fichier
            $query = $db_connect->prepare('INSERT INTO files(_id,id_owner,title,name,description) VALUES (:_id,:owner,:title,:name,:description)'); 
            $result = $query->execute(array(
                '_id'=>$id,
                'owner'=>$_SESSION['id'],
                'title'=>$tabAgs['title'],
                'name'=>$tabAgs['name'],
                'description'=>$tabAgs['description']
                )
            );
            if(!$result){
                throw new Exception("<br>PDO::errorInfo():<br> Erreur durant la récupération du fichier<br>");
            }
            return true;
        } catch  (Exception $e){
            echo $e->getMessage();
            var_dump($db_connect->errorInfo());
            die($e->getMessage());
        }
    }

    function modificationAnnonce($db_connect,$tabAgs){
        try{
            $query = $db_connect->prepare('UPDATE annonces SET creatorID=:creatorID, titre=:titre, description=:description, prix=:prix, endDate=:endDate WHERE _id=:_id'); 
            return ($query->execute(array(
                "creatorID" => $tabAgs['creatorID'],
                "titre" => $tabAgs['titre'],
                "description" => $tabAgs['description'],
                "prix" => floatval($tabAgs['prix']),
                "endDate" => date("Y-m-d", strtotime($tabAgs['endDate'])),
                "_id" => $tabAgs['_id']
                )
            ))? true : false;
        } catch  (Exception $e){
            die($e->getMessage());
        }
    }

    function suppressFile($db_connect,$id){
        #code...
        try {
            $query = $db_connect->prepare('DELETE FROM files WHERE _id = :id'); 
            $result= $query->execute(array("id" => $id));
            if(!$result){
                throw new Exception("<br>PDO::errorInfo():<br> Erreur durant la suppression du fichier<br>");
            }
            return $result;
        } catch  (Exception $e){
            echo $e->getMessage();
            var_dump($db_connect->errorInfo());
            die($e->getMessage());
        }
    }
?>