<?php
    require_once('./model/fullMethode.php');
    #BDD parameter connection
    $bdd_name = "ProjetPHP";
    $bdd_port = "3306";
    $bdd_host = "localhost";
    $bdd_username = "root";
    $bdd_password = "";
    
    
    function bddConnect($bdd_name , $bdd_port , $bdd_host , $bdd_username , $bdd_password){
        try {
            $connexion = new PDO('mysql:host='. $bdd_host .';dbname='. $bdd_name . ';user=' . $bdd_username . 'password=' . $bdd_password );
            return $connexion;
        }catch  (Exception $e){
            die($e->getMessage());
        }
    }
?>