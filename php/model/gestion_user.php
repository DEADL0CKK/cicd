<?php
require_once('./model/bdd_connexion.php');

function allUsers($db_connect)
{
    #code
    try {
        $query = $db_connect->prepare('SELECT * FROM users');
        if (!$query->execute()) {
            throw new Exception("<br>PDO::errorInfo():<br> Erreur durant la récupération des fichiers<br>");
        }
        return $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        echo $e->getMessage();
        var_dump($db_connect->errorInfo());
        die($e->getMessage());
    }
}

function recupInfoUser($db_connect)
{
    #code
    $db_connect->beginTransaction();
    try {
        $query = $db_connect->prepare('SELECT * FROM users WHERE _id = :id ');
        if (!$query->execute(array('id' => $_SESSION['id']))) {
            throw new Exception("<br>PDO::errorInfo():<br> Erreur durant la récupération des données de l'utilisateur<br>");
        }
        $array = $query->fetch(PDO::FETCH_ASSOC);
        $query = $db_connect->prepare('SELECT * FROM users_pictures WHERE id_user = :id ');
        if (!$query->execute(array('id' => $_SESSION['id']))) {
            throw new Exception("<br>PDO::errorInfo():<br> Erreur durant la récupération des données de l'image de l'utilisateur<br>");
        }
        $array['picture'] = $query->fetch(PDO::FETCH_ASSOC);
        if ($db_connect->commit()) {
            return $array;
        } else {
            throw new Exception('<br>PDO::errorInfo():<br> Erreur durant la récupération des données<br>');
        }
    } catch (Exception $e) {
        echo $e->getMessage();
        var_dump($db_connect->errorInfo());
        $db_connect->rollback();
        die($e->getMessage());
    }
}


function modifUser($db_connect, $infoUser)
{
    #code
    $db_connect->beginTransaction();
    try {
        $query = $db_connect->prepare('UPDATE users SET login=:login, password=:password, name=:name, firstname=:firstname, email=:email, description=:description  WHERE _id = :id ');
        if (!$query->execute(array(
            'id' => $_SESSION['id'],
            'login' => $infoUser['login'],
            'password' => $infoUser['password'],
            'name' => $infoUser['name'],
            'firstname' => $infoUser['firstname'],
            'email' => $infoUser['email'],
            'description' => $infoUser['description']
        ))) {
            throw new Exception("<br>PDO::errorInfo():<br> Erreur durant la modification des données<br>");
        }
        $query = $db_connect->prepare('UPDATE users_pictures SET name=:name WHERE _id = :id ');
        if (!$query->execute(array(
            'id' => $infoUser['profilPicture'],
            'name' => $infoUser['picture']
        ))) {
            throw new Exception("<br>PDO::errorInfo():<br> Erreur durant la modification des données<br>");
        }
        return $db_connect->commit()?true:false;
    } catch (Exception $e) {
        echo $e->getMessage();
        var_dump($db_connect->errorInfo());
        die($e->getMessage());
    }
}
