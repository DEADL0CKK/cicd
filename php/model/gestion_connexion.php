<?php
    require_once('bdd_connexion.php');
    
    function inscription($db_connect,$tabAgs){
        #code
        $db_connect->beginTransaction();
        try{
            #1st block : Créer un id pour le nouvel utilisateur
            $query = $db_connect->prepare('SELECT count(*) as userNumber FROM users');
            $query->execute();
            $searchnbuser = $query->fetch(PDO::FETCH_ASSOC);
            $id_users = $searchnbuser? $searchnbuser['userNumber']+1 : 1;
            $query = $db_connect->prepare('SELECT count(*) as userNumber FROM users_pictures');
            $query->execute();
            $searchnbuser = $query->fetch(PDO::FETCH_ASSOC);
            $id_pictures = $searchnbuser? $searchnbuser['userNumber']+1 : 1;
            #2nd block: ajouter une ligne dans users_pictures
            $query = $db_connect->prepare('INSERT INTO users_pictures(_id,id_user) VALUES (?,?)'); 
            $result = $query->execute(array(
                intval($id_pictures),
                intval($id_users))
            );
            if (!$result) {
                throw new Exception("<br>PDO::errorInfo():<br> Erreur durant l'enregistrement dans users_pictures<br>");
            }
            #3rd block : créer un utilisateur
            $query = $db_connect->prepare('INSERT INTO users(_id,login,password,email,description,signInDate,profilPicture) VALUES (?,?,?,?,?,?,?)'); 
            $result = $query->execute(array(
                intval($id_users),
                $tabAgs['login'],
                $tabAgs['password'],
                $tabAgs['email'],
                $tabAgs['description'],
                $tabAgs['signInDate'],
                intval($id_pictures))
            );
            if (!$result) {
                throw new Exception("<br>PDO::errorInfo():<br> Erreur durant l'enregistrement dans users<br>");
            }
            #4th block : return statement
            return ($db_connect->commit())? true : false;
        } catch  (Exception $e){
            echo $e->getMessage();
            var_dump($db_connect->errorInfo());
            $db_connect->rollback();
            die;
        }
        
    }

    function connexion($db_connect,$login,$pwd){
        #code
        try {
            $query = $db_connect->prepare('SELECT * FROM users WHERE login = ? AND password = ? '); 
            return ($query->execute(array($login,$pwd)))? $query->fetch(PDO::FETCH_ASSOC): false;
        } catch  (Exception $e){
            die($e->getMessage());
        }
        
    }
        
?>