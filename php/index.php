<?php
    session_start();
    $args = "";
    if(!empty($_GET)){
        foreach($_GET as $key => $value){
            if(strlen($args) > 1) $args .= "&".$key."=".$value;
            else $args = $key."=".$value;
        }
    }
    $url = "./controller/defaultController.php?".$args;
    $_SESSION['dir'] = __DIR__;
    $_SESSION['args'] = $args;
    $_SESSION['uploads_dir_nc'] = $_SESSION['dir'].'\\file\\';
    $action = array_key_first($_GET);

    if(session_status() == PHP_SESSION_ACTIVE){
        switch ($action) {
            case 'user':
                # code...
                require_once('./controller/userController.php');
                break;
            case 'deconnexion':
                # code...
                header('Location: index.php?connexion=deconnexion');
                break;
            case 'file':
                # code...
                require_once('./controller/fileController.php');
                break;
            case 'connexion':
                # code...
                require_once('./controller/connexionController.php');
                break;
            case 'action':
                # code...
                require_once('./controller/defaultController.php');
                break;
            default:
                # code...
                require_once('./controller/defaultController.php');
                break;
        }
    } else {
        #Gestion d'erreur indiquer le fait de ne pas être connecter et rediriger l'utilisateur
        echo("Vous n'êtes pas connecter, vous ne pouvez pas progressdans l'application");
    }
    
?>
