<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page de connexion</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div id="connexion" name="connexion">
        <div style="width: 50%; display:inline-block;" class="bottomBorder">
            <label for="form_connection">Connection : </label>
            <form name="form_connection" id="form_connection" method="POST" action="../index.php?connexion=connexion" class="topBorder">
                <label for="username">Username : </label><input type="text" name="username" id="username">
                <label for="password">Password : </label><input type="password" name="password" id="password">
                <br>
                <input type="submit" value="Connect">
            </form>
            <a href="../index.php?action=inscription"><button>Sign in</button></a>
        </div>
    </div>
</body>
</html>