<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php session_start() ?>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div style="display: block;border-bottom:1px solid black;margin :5px;">
        <a href="../index.php?action=accueil"><button style="margin:5px;"> Accueil</button></a>
    </div>
    <div style="display: block;" id="profil_utilisateur" name="profil_utilisateur">
        <form action="../index.php?user=modification&id=<?php echo $_SESSION['id'] ?>" method="post" enctype="multipart/form-data">
            <?php
                foreach($_SESSION['oneUser'] as $key => $value){
                    switch ($key) {
                        case 'age':
                            # code...
                            $type = 'number';
                            echo("
                                <label for='".$key."'>".$key." : </label>
                                <input type='".$type."' id='".$key."' name='".$key."' value='".$value."'>
                                <br></br>
                            ");
                            break;
                        case 'picture':
                            # code...
                            $type = 'file';
                            if(isset($value['name'])){
                                echo("
                                <img style='width:300px' src ='..\\file\\users\\".$_SESSION['id']."\\".$value['name']."'/> <br>
                                <label for='".$key."'> Change your ".$key." : </label>
                                <input type='hidden' id='".$key."' name='".$key."' value='".$value['_id']."'>
                                <input type='".$type."' id='".$key."_file' name='".$key."_file'>
                                <br></br>
                            ");
                            } else {
                                echo("
                                <label for='".$key."'> Change your ".$key." : </label>
                                <input type='hidden' id='".$key."' name='".$key."' value='".$value['_id']."'>
                                <input type='".$type."' id='".$key."_file' name='".$key."_file'>
                                <br></br>
                            ");
                            }
                            
                            break;
                        case 'profilPicture':
                            # code...
                            echo("
                                <input type='hidden' id='".$key."' name='".$key."' value='".$value."'>
                            ");
                            break;
                        case '_id':
                            # code...
                            echo("
                                <input type='hidden' id='".$key."' name='".$key."' value='".$value."'>
                            ");
                            break;
                        case 'signInDate':
                            # code...
                            $type = 'date';
                            echo("
                                <label for='".$key."'>".$key." : </label>
                                <input disabled type='".$type."' id='".$key."' name='".$key."' value='".$value."'>
                                <br></br>
                            ");
                            break;
                        default:
                            # code...
                            $type = 'text';
                            echo("
                                <label for='".$key."'>".$key." : </label>
                                <input type='".$type."' id='".$key."' name='".$key."' value='".$value."'>
                                <br></br>
                            ");
                            break;
                    }                    
                }
            ?>
            <input type="submit" value="Modifier le profil">
        </form>
    </div>
</body>
</html>