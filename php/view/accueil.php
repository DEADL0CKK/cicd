<!DOCTYPE html>
<html lang="en">
<head>
    <?php session_start(); ?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div style="width: 100%; display:block;">
        <a href="../index.php?deconnexion"><button style="margin-left:auto;margin-right:auto;">Déconnexion</button></a>
        <a href="../index.php?user=affichage"><button style="margin-left:auto;margin-right:auto;">Mon Profil</button></a>
    </div>
    
    <div style="width: 100%; display:block; margin-left:auto;margin-right:auto;" id="creationAnnonce" name="creationAnnonce" class="bottomBorder">
        <label for="form_connection">Création dépôt de fichier : </label>
        <form name="form_connection" id="form_connection" method="POST" action="../index.php?file=creation" class="topBorder" enctype="multipart/form-data"><br>
            <label for="username">Titre : </label><input type="text" name="title" id="title"><br>
            <label for="description">Description : </label><br><textarea name="description" id="description" cols="52" rows="10"></textarea><br>
            <label for="password">Fichier : </label><input type="file" name="file" id="file"><br>
            <input style="display:block;margin: 5px;" type="submit" value="Création">
        </form>
    </div>
    <br>
    <div style="width: 100%; display:block;" id="annonces" name="annonces">
        <div style="width: 45%; display:inline-block;margin-top:auto;margin-bottom:auto;" id="mesAnnonces" name="mesAnnonces" class="bottomBorder">
            <label for="affichage_annonce">Mes fichiers : </label>
            <div id="affichage_annonce" name="affichage_annonce" class="topBorder">
                <?php
                    foreach($_SESSION['allFiles'] as $key => $file){
                        if($file['id_owner'] == $_SESSION['id'])
                            echo("<a href='../index.php?file=details&id=".$file['_id']."'>".$file['title']."</a><br>");
                    }
                ?>
            </div>
        </div>
        <div style="width: 45%; display:inline-block;margin-top:auto;margin-bottom:auto;" id="allAnnonce" name="allAnnonce" class="bottomBorder">
            <label for="affichage_annonce">Liste des fichiers : (affiche les 5 premiers disponibles) </label>
            <div id="affichage_annonce" name="affichage_annonce" class="topBorder">
                <?php
                    $cpt = 0;
                    foreach($_SESSION['allFiles'] as $key => $file){
                        echo("<a href='../index.php?file=details&id=".$file['_id']."'>".$file['title']."</a><br>");
                        if($cpt >= 5){
                            break;
                        } else {
                            $cpt++;
                        }
                    }
                ?>
                <a href="../index.php?file=allFile"><button>Voir tous les fichiers</button></a>
            </div>
        </div>
    </div>
    
</body>
</html>