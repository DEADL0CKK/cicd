<?php
    $action = $_GET['action'];
    switch ($action) {
        case 'inscription':
            # code...
            if(!isset($_SESSION['args'])){
                header('Location: ./view/page_inscription.php');
            } else {
                header('Location: ./view/page_inscription.php?'.$_SESSION['args']);
            }
            break;
        case 'connexion':
            # code...
            if(!isset($_SESSION['args'])){
                header('Location: ./view/page_connexion.php');
            } else {
                header('Location: ./view/page_connexion.php?'.$_SESSION['args']);
            }            
            break;
            
        case 'accueil':
            # code...
            require_once('./model/gestion_file.php');
            $connexion = bddConnect($bdd_name , $bdd_port , $bdd_host , $bdd_username , $bdd_password);
            $_SESSION['allFiles'] = allFiles($connexion);
            header('Location: ./view/accueil.php?'.$_SESSION['args']);
            break;
        default:
            # code...
            header('Location: ./view/page_connexion.php');
            // header('Refresh:15, url="../view/page_connexion.php"');
            break;
    } 
?>