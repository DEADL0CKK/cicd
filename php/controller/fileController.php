<?php    
    $action = $_GET['file'];
    switch ($action) {
        case 'suppression':
            # code...
            require_once("./model/gestion_file.php");

            #POST Variable
            $connexion = bddConnect($bdd_name , $bdd_port , $bdd_host , $bdd_username , $bdd_password);
            $uploads_dir = $_SESSION['uploads_dir_nc'] . 'files\\' . $_SESSION['oneFile']['id_owner'];
            $_SESSION['oneFile'] = oneFile($connexion, $_GET['idFile']);
            if(isset($_SESSION['oneFile']['name']) && file_exists($uploads_dir."\\".$_SESSION['oneFile']['name'])){
                unlink($uploads_dir."\\".$_SESSION['oneFile']['name']);
            }
            if(suppressFile($connexion,$_GET['idFile'])){
                header("Location: ./index.php?action=accueil&success=true");
            } else {
                header("Location: ./index.php?action=accueil&success=false");
            }
            break;
        case 'download':
            require_once("./model/gestion_file.php");
            $connexion = bddConnect($bdd_name , $bdd_port , $bdd_host , $bdd_username , $bdd_password);
            $file = oneFile($connexion,$_GET['idFile']);
            $thisFile = $_SESSION['uploads_dir_nc']."files\\".$file['id_owner'].'\\'.$file['name'];
            if (file_exists($thisFile)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($thisFile).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($thisFile));
                readfile($thisFile);
                exit;
            }
            header('Location: ./index.php?file=allFile');
            break;
        case 'allFile':
            require_once("./model/gestion_file.php"); require_once("./model/gestion_user.php");
            $connexion = bddConnect($bdd_name , $bdd_port , $bdd_host , $bdd_username , $bdd_password);
            $_SESSION['allFiles'] = allFiles($connexion);
            $_SESSION['allUsers'] = [];

            foreach(allUsers($connexion) as $key => $user){
                $_SESSION['allUsers'][$user['_id']] = $user;
            }
            header('Location: ./view/file.php');
            break;
        case 'creation':
            # code...
            require_once("./model/gestion_file.php");

            #POST Variable
            $connexion = bddConnect($bdd_name , $bdd_port , $bdd_host , $bdd_username , $bdd_password);
            $uploads_dir = $_SESSION['uploads_dir_nc'] . 'files\\' . $_SESSION['id'];
            verificationFolder($_SESSION['uploads_dir_nc']. 'files\\',$_SESSION['id']);
            if ($_FILES["file"]["error"] === UPLOAD_ERR_OK) {
                $tmp_name = $_FILES["file"]["tmp_name"];
                $name = $_FILES["file"]["name"];
                move_uploaded_file($tmp_name, "$uploads_dir\\$name");
            }
            $infoFile = $_POST;
            $infoFile['name'] = $name;
            if(creationFile($connexion,$infoFile)){
                header("Location: ./index.php?action=accueil&success=true");
            } else {
                header("Location: ./index.php?action=accueil&success=false");
            }
            break;

        case 'details':
            require_once("./model/gestion_file.php"); require_once("./model/gestion_user.php");
            $connexion = bddConnect($bdd_name , $bdd_port , $bdd_host , $bdd_username , $bdd_password);
            $_SESSION['oneFile'] = oneFile($connexion, $_GET['id']);
            $_SESSION['allUsers'] = [];
            foreach(allUsers($connexion) as $key => $user){
                $_SESSION['allUsers'][$user['_id']] = $user;
            }
            header('Location: ./view/oneFile.php');
            break;

        case 'modification' :
            require_once("./model/gestion_file.php");
            $connexion = bddConnect($bdd_name , $bdd_port , $bdd_host , $bdd_username , $bdd_password);
            $uploads_dir_nc = $_SESSION['dir'].'\\file\\files\\';
            $uploads_dir = $uploads_dir_nc . $_SESSION['id'];
            verificationFolder($uploads_dir_nc,$_SESSION['id']);
            if ($_FILES["file"]["error"] === UPLOAD_ERR_OK) {
                $tmp_name = $_FILES["file"]["tmp_name"];
                $name = $_FILES["file"]["name"];
                move_uploaded_file($tmp_name, "$uploads_dir\\$name");
            }
            if(isset($_SESSION['oneFile']['file']['file']) && file_exists($uploads_dir."\\".$_SESSION['oneFile']['file']['file'])){
                unlink($uploads_dir."\\".$_SESSION['oneFile']['file']['file']);
            }
            $infoFile = $_POST;
            $infoFile['name'] = $name;
            if(modificationAnnoncePicture($connexion,$infoFile)){
                header("Location: ./index.php?action=accueil&modification=success");
            } else {
                header("Location: ./index.php?action=accueil&modification=failure");
            }
            
            break;

        default:
            # code...
            break;
    }

    /** */

    
?>