<?php 

    $action = $_GET['connexion'];
    switch ($action) {
        case 'deconnexion':
            # code...
            $_SESSION['args'] = "";
            session_destroy();
            header('Location: index.php');
            // header('Refresh:15, url="../view/page_connexion.php"');
            break;
        case 'connexion':
            # code...
            require_once('./model/gestion_connexion.php');
            $connexion = bddConnect($bdd_name , $bdd_port , $bdd_host , $bdd_username , $bdd_password);
            if($connexion) {
                $username = $_POST['username'];
                $password = $_POST['password'];
                $utilisteur = connexion($connexion,$username,$password);

                if($utilisteur){
                    
                    $_SESSION['id'] = htmlspecialchars($utilisteur['_id']);
                    $_SESSION['login'] = htmlspecialchars($utilisteur['login']);
                    $_SESSION['email'] = htmlspecialchars($utilisteur['email']);
                    header('Location: index.php?action=accueil');

                } else {
                    session_destroy();
                    header('Location: index.php?connect=false');
                }
            }
            break;
        case 'inscription':
            # code...
            require_once('./model/gestion_connexion.php');
            $connexion = bddConnect($bdd_name , $bdd_port , $bdd_host , $bdd_username , $bdd_password);
            if( !preg_match ( " /^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/ " , $_POST['email'])){
                echo "L'adresse email renseigné ne respect pas le format !";
                echo "<a href='../index.php'><button></button></a>";
                die;
            }
            if($connexion) {
                $info_user= [
                    'login' => $_POST['login'],
                    'password' => $_POST['password'],
                    'email' => $_POST['email'],
                    'description' => $_POST['description'],
                    'signInDate' => date("Y-m-d"),
                ];
                $connect = inscription($connexion,$info_user);
                $utilisateur = connexion($connexion,$info_user['login'],$info_user['password']);
                if($utilisateur){
                    $_SESSION['id'] = htmlspecialchars($utilisateur['_id']);
                    $_SESSION['login'] = htmlspecialchars($utilisateur['login']);
                    $_SESSION['email'] = htmlspecialchars($utilisateur['email']);
                    header('Location: ./index.php?action=accueil');
                } else {
                    header('Location: ./index.php?action=inscription&inscription=false');
                }
            }
            break;
        default:
            # code...
            if(isset($_SESSION['args']))
                header('Location: ./view/page_connexion.php?'.$_SESSION['args']);
            else
                header('Location: ./view/page_connexion.php');
            // header('Refresh:15, url="../view/page_connexion.php"');
            break;
    } 

?>