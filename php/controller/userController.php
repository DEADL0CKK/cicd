<?php
        $action = $_GET['user'];
        $uploads_dir = $_SESSION['uploads_dir_nc'] . 'users\\' . $_SESSION['id'];
        switch ($action) {
            case 'modification':
                # code...
                require_once('./model/gestion_user.php');
                $connexion = bddConnect($bdd_name , $bdd_port , $bdd_host , $bdd_username , $bdd_password);
                $infoUser = $_POST;
                verificationFolder($_SESSION['uploads_dir_nc']. 'users\\',$_SESSION['id']);
                if ($_FILES["picture_file"]["error"] === UPLOAD_ERR_OK) {
                    $tmp_name = $_FILES["picture_file"]["tmp_name"];
                    $name = $_FILES["picture_file"]["name"];
                    move_uploaded_file($tmp_name, "$uploads_dir\\$name");
                }
                if(isset($_SESSION['oneUser']['picture']['name']) && file_exists($uploads_dir."\\".$_SESSION['oneUser']['picture']['name'])){
                    unlink($uploads_dir."\\".$_SESSION['oneUser']['picture']['name']);
                }
                $infoUser['picture'] = $name;
                if(modifUser($connexion,$infoUser)){
                    $_SESSION['oneUser'] = recupInfoUser($connexion);
                }
                header('Location: ./view/profilUtilisateurs.php?'.$_SESSION['args']);
                break;
            case 'affichage':
                # code...
                require_once('./model/gestion_user.php');
                $connexion = bddConnect($bdd_name , $bdd_port , $bdd_host , $bdd_username , $bdd_password);
                $_SESSION['oneUser'] = recupInfoUser($connexion);
                $uploads_dir = $_SESSION['uploads_dir_nc'] . $_SESSION['id'];
                verificationFolder($_SESSION['uploads_dir_nc']. 'users\\',$_SESSION['id']);
                header('Location: ./view/profilUtilisateurs.php?'.$_SESSION['args']);
                break;
            default:
                # code...
                header('Location: ./view/page_connexion.php');
                // header('Refresh:15, url="../view/page_connexion.php"');
                break;
        } 
?>